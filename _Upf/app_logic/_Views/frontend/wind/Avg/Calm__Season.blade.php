<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>

<body>

<table border="1">
    <tr>
        <td>Год/Сезон</td>
        @for($i=1; $i<=4;$i++)
            <td>{{$i}}</td>
        @endfor
    </tr>
    @foreach($WindMode['SeasonCalm'] as $YearKey=>$Year)
        <tr>
            <td>{{$YearKey}}</td>
            @foreach($Year as $Month)
                @if(is_numeric($Month))
                    <td>{{str_replace('.',',',round($Month,2))}}</td>
                @else
                    <td></td>
                @endif
            @endforeach
        </tr>
    @endforeach
</table>

</body>
</html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>

<table border="1">
    <tr>
        <td>Месяц/Направление</td>
        @foreach($WindMode['Directions'] as $Direction)
            @if($Direction != 'Ш')
                <td>{{$Direction}}</td>
            @endif
        @endforeach
    </tr>

    @foreach($WindMode['DirSpeedYear'] as $YearKey => $Year )
        <tr>
            <td>{{$YearKey}}</td>
            @foreach($WindMode['Directions'] as $Direction)
                @if($Direction != 'Ш')
                    <td>{{round($Year[$Direction],1)}}</td>
                @endif
            @endforeach
        </tr>
    @endforeach
    <tr>
        <td>Результат</td>
        @foreach($WindMode['Directions'] as $Direction)
            @if($Direction != 'Ш')
                <td>{{round($WindMode['DirSpeedAllTime'][$Direction],1)}}</td>
            @endif
        @endforeach
    </tr>

</table>

</body>
</html>
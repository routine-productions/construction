<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
     @foreach($WindMode['SeasonPercent'] as $YearKey => $Year )
     <h3>За {{$YearKey}}</h3>
     <table border="1">

         <tr>
             <td>Сезон/Направление</td>
             @foreach($WindMode['Directions'] as $Direction)
                 <td>{{$Direction}}</td>
             @endforeach
         </tr>

        @foreach($Year as $MonthKey => $Month)
         <tr>
             <td>{{$MonthKey}}</td>

             @foreach($WindMode['Directions'] as $Direction)
                    <td>{{isset($Month[$Direction])?str_replace('.',',',round($Month[$Direction],3)*100):0}}</td>
             @endforeach
         </tr>
        @endforeach
     </table>
     @endforeach
</body>
</html>
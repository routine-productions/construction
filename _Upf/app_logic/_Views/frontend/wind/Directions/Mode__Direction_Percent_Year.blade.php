<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>

     <table border="1">
         <tr>
             <td>Год/Направление</td>
             @foreach($WindMode['Directions'] as $Direction)
                 <td>{{$Direction}}</td>
             @endforeach
         </tr>

         @foreach($WindMode['YearPercent'] as $YearKey => $Year )
         <tr>
             <td>{{$YearKey}}</td>
             @foreach($WindMode['Directions'] as $Direction)
                    <td>{{isset($Year[$Direction])?str_replace('.',',',round($Year[$Direction],3)*100):0}}</td>
             @endforeach
         </tr>
         @endforeach
     </table>

</body>
</html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>

<table border="1">
    <tr>
        <td>Месяц/Направление</td>
        @foreach($WindMode['Directions'] as $Direction)
            @if($Direction != 'Ш')
                <td>{{$Direction}}</td>
            @endif
        @endforeach
    </tr>

    @foreach($WindMode['DirSpeedSeason'] as $YearKey => $Year )
        @foreach($Year as $MonthKey => $Month )
            <tr>
                <td>{{$YearKey}}-{{$MonthKey}}</td>
                @foreach($WindMode['Directions'] as $Direction)
                    @if($Direction != 'Ш')
                        <td>{{round($Month[$Direction],1)}}</td>
                    @endif
                @endforeach
            </tr>
        @endforeach
    @endforeach
</table>

</body>
</html>
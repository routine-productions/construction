<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
<table border="1">
    <tr>
        @foreach($WindMode['Winds'] as $Wind)
            <td>{{$Wind}}</td>
        @endforeach
    </tr>

    <tr>

        @foreach($WindMode['Winds'] as $Wind)
            <td>{{isset($WindMode['VelocityTimesAllTime'][$Wind])?str_replace('.',',',round($WindMode['VelocityTimesAllTime'][$Wind],3)*100):0}}</td>
        @endforeach
    </tr>

</table>
</body>
</html>
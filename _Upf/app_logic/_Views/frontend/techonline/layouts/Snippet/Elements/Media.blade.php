<div class="Item-Gallery {{$MediaClass}}">


    @if( !empty($Item['logotype'] ) )
        <a href="{{ $Item['logotype'] }}" rel="Gallery-{{$Item['id']}}" class="fancybox">
            <img class="Item-Main-Photo" src="{{ $Item['logotype'] }}" alt="{{$Item['title']}}">
        </a>
    @elseif( !empty($Item['catalog']) )
        <a href="{{ $Item['catalog']['logotype'] }}" rel="Gallery-{{$Item['id']}}" class="fancybox">
            <img class="Item-Main-Photo" src="{{ $Item['catalog']['logotype'] }}" alt="{{$Item['title']}}">
        </a>
    @endif
    <ul>
        @if( isset($Item['meta']['files']) )


            @foreach( $Item['meta']['files'] as $PhotoKey => $Photo)


                @if( $PhotoKey >= 0 && $PhotoKey < 4 )
                    <li>
                        <a href="{{$Photo['src']}}" rel="Gallery-{{$Item['id']}}" class="fancybox">
                            <img src="{{$Photo['src']}}" alt="{{$Photo['title']}}">
                        </a>
                    </li>


                @elseif( $PhotoKey >= 4 )
                    <li style="display: none">
                        <a href="{{$Photo['src']}}" rel="Gallery" class="fancybox">
                            <img src="{{$Photo['src']}}" alt="{{$Photo['title']}}">
                        </a>
                    </li>
                @endif

            @endforeach
        @endif
    </ul>
</div>